import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {

        // 1 Cho một chuỗi str, xoá các ký tự xuất hiện nhiều hơn một lần trong chuỗi và
        // chỉ giữ lại ký tự đầu tiên
        char str1[] = "bananas".toCharArray();
        int n1 = str1.length;
        System.out.println("1.remove duplicate: " + removeDuplicate(str1, n1));

        // 2 Cho một xâu kí tự, đếm số lượng các từ trong xâu kí tự đó ( các từ có thể
        // cách nhau bằng nhiều khoảng trắng )
        String words2 = "Devcamp java";
        int countWords = words2.split("\\s").length;
        System.out.println("2. Count Number of Words in a String: " + countWords);

        // 3 Cho hai xâu kí tự s1, s2 nối xâu kí tự s2 vào sau xâu s1

        String first = "abc";
        String second = "def";

        String third = first + second;
        System.out.println("3: concatenate two strings: " + third);

        // yet another way to concatenate strings
        first += second;
        System.out.println("3: concatenate two strings: " + first);

        String first3 = "abc";
        String second3 = "def";

        // use concat()
        String third3 = first3.concat(second3);
        System.out.println("3: concatenate two strings use concat(): " + third3);

        // 4.Cho hai xâu kí tự s1, s2, kiểm tra xem chuỗi s1 chứa chuỗi s2 không?
        String myStr4 = "Devcamp java";
        System.out.println("4." + myStr4.contains("java"));

        // 5 Hiển thị ký tự thứ k trong string
        String name5 = "Devcamp java";
        char ch = name5.charAt(3);// returns the char value at the 3rd index
        System.out.println("5.returns the char value at the 3rd index: " + ch);

        // 6 đếm số lần xuất hiện của một ký tự trong một xâu
        String str6 = "Devcamp java";
        char word6 = 'a';
        System.out.println("6. Count Occurrences of letter a: " + countOccurrences(str6, word6));

        // 7 Tìm vị trí xuất hiện lần đầu tiên của một ký tự trong một xâu
        String str7 = "Devcamp java";
        char c7 = 'a';
        int index7 = findFirstOccurrence(str7, c7);
        System.out.println("7. Find index of a: " + index7); // 5

        // 8 Chuyển các ký tự in thường sang in hoa
        String str8 = "Devcamp";
        System.out.println("8. Uppercase String: " + str8.toUpperCase());

        // 9 Chuyển các ký tự in thường sang in hoa và ngược lại
        String str9 = "DevCamp";
        String res9 = swapCase(str9);
        System.out.println("9. Swap Case of DevCamp is: " + res9);

        // 10 Đếm số ký tự in hoa trong một xâu
        String str10 = "DevCamp123";
        int count10 = countUpperCase(str10);
        System.out.println("10. Count uppercase letters in string: " + count10);

        // 11 Hiển thị ra màn hình các ký tự từ A tới Z
        String str11 = "DevCamp123";
        String result11 = getUppercaseCharacters(str11);
        System.out.println("11:" + result11); // in ra "DC"

        // 12 Cho một chuỗi str và số nguyên n >= 0. Chia chuỗi str ra làm các phần bằng
        // nhau với n ký tự. Nếu chuỗi không chia được thì xuất ra màn hình “KO”.
        String str12 = "abcdefghijklmnopqrstuvwxy";
        int n12 = 5;
        String[] parts12 = splitString(str12, n12);
        System.out.println("12. Split string: " + Arrays.toString(parts12)); // in ra "[KO]"

        // 13 Xoá tất cả các ký tự liền kề và giống nhau
        String str13 = "aabaarbarccrabmq";
        String result13 = removeAdjacentDuplicates(str13);
        System.out.println("13. Remove duplicate: " + result13); // in ra "abc"

        // 14 Cho 2 string, gắn chúng lại với nhau, nếu 2 chuỗi có độ dài không bằng
        // nhau thì tiến hành cắt bỏ các ký tự đầu của string dài hơn cho đến khi chúng
        // bằng nhau thì tiến hành gắn lại
        String s141 = "Welcome";
        String s142 = "home";
        String result14 = concatenateStringsSubTask14(s141, s142);
        System.out.println("14. Concatenate string: " + result14); // in ra " chaothe gioi"

        // 15
        // 16 Kiểm tra xem một chuỗi có xuất hiện số hay không
        String str16inp1 = "DevCamp123";
        boolean res16inp1 = containsNumber(str16inp1);
        System.out.println("16. String DevCamp123 contains number: " + res16inp1);

        String str16inp2 = "DevCamp";
        boolean res16inp2 = containsNumber(str16inp2);
        System.out.println("16. String DevCamp contains number: " + res16inp2);


        // 17 Kiểm tra chuỗi này có phù hợp với yêu cầu hay không Yêu cầu về chuỗi là:
        // Có độ dài không quá 20 ký tự, không được chứa ký tự khoảng trắng, bắt đầu
        // bằng một ký tự chữ viết hoa (A - Z), kết thúc bằng một số (0 - 9). (Sử dụng
        // biểu thức chính quy để ràng buộc định dạng)
        String regex17 = "^[A-Z][^\\s]{0,18}[0-9]$";
        String input17 = "DevCamp123";
        boolean isValid17 = input17.matches(regex17);
        System.out.println("17. Check input DevCamp123: " + isValid17);

        String inp17 = "DEVCAMP 123";
        boolean isValid = inp17.matches(regex17);
        System.out.println("17. Check input DEVCAMP 123: " + isValid);

    }

    // 1
    public static String removeDuplicate(char str[], int n) {
        // Used as index in the modified string
        int index = 0;

        // Traverse through all characters
        for (int i = 0; i < n; i++) {

            // Check if str[i] is present before it
            int j;
            for (j = 0; j < i; j++) {
                if (str[i] == str[j]) {
                    break;
                }
            }

            // If not present, then add it to
            // result.
            if (j == i) {
                str[index++] = str[i];
            }
        }
        return String.valueOf(Arrays.copyOf(str, index));
    }

    // 6
    public static int countOccurrences(String str, char word) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == word) {
                count++;
            }
        }
        return count;

    }

    // 7
    public static int findFirstOccurrence(String str, char c) {
        return str.indexOf(c);

    }

    // 9
    public static String swapCase(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLowerCase(c)) {
                sb.append(Character.toUpperCase(c));
            } else if (Character.isUpperCase(c)) {
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // 10
    public static int countUpperCase(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (Character.isUpperCase(str.charAt(i))) {
                count++;
            }
        }
        return count;
    }

    // 11
    public static String getUppercaseCharacters(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // 12
    public static String[] splitString(String str, int n) {
        if (n <= 0 || str.length() % n != 0) {
            return new String[] { "KO" };
        }
        int numParts = str.length() / n;
        String[] parts = new String[numParts];
        for (int i = 0; i < numParts; i++) {
            parts[i] = str.substring(i * n, (i + 1) * n);
        }
        return parts;
    }

    // 13
    public static String removeAdjacentDuplicates(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (sb.length() == 0 || sb.charAt(sb.length() - 1) != c) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // 14
    public static String concatenateStringsSubTask14(String s1, String s2) {
        if (s1.length() > s2.length()) {
            s1 = s1.substring(s1.length() - s2.length());
        } else if (s2.length() > s1.length()) {
            s2 = s2.substring(s2.length() - s1.length());
        }
        return s1 + s2;
    }

    // 16
    public static boolean containsNumber(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

}
